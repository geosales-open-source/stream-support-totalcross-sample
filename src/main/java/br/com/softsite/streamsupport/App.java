package br.com.softsite.streamsupport;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.function.BiFunction;

import br.com.softsite.toolbox.collection.CollectionUtils;
import br.com.softsite.toolbox.stream.Stream;
import totalcross.ui.Button;
import totalcross.ui.Container;
import totalcross.ui.Edit;
import totalcross.ui.Label;
import totalcross.ui.MainWindow;

public class App extends MainWindow {
	public App() {
	}

	@Override
	public void initUI() {
		Container c = new Container();
		LinkedHashMap<String, String> hash = new LinkedHashMap<>();
		hash.put("hash", "map");
		Button btn = new Button("Hello world?");

		btn.addPressListener(e -> {
			HashMap<String, String> hash2 = CollectionUtils.getHashMap(hash);
			c.removeAll();
			c.add(new Label("resultados"), LEFT, TOP, FILL, PREFERRED);
			new Stream<>(hash2.entrySet()).map(this::serializeES).forEach(s -> {
				Label l = new Label(s);
				c.add(l, LEFT, AFTER, FILL, PREFERRED);
			});
		});

		Button btn2 = new Button("Hello world direct?");

		btn2.addPressListener(e -> {
			c.removeAll();
			c.add(new Label("resultados direto"), LEFT, TOP, FILL, PREFERRED);
			new Stream<>(hash.entrySet()).map(this::serializeES).forEach(s -> {
				Label l = new Label(s);
				c.add(l, LEFT, AFTER, FILL, PREFERRED);
			});
		});

		Button btn3 = new Button("Hello world bifunction?");

		btn3.addPressListener(e -> {
			c.removeAll();
			c.add(new Label("resultados bifunction"), LEFT, TOP, FILL, PREFERRED);
			new Stream<>(hash.entrySet()).map(this::serializeES).forEach(s -> {
				BiFunction<String, Container, Label> f = (s1, c1) -> {
					Label l = new Label(s1);
					c1.add(l, LEFT, AFTER, FILL, PREFERRED);
					return l;
				};
				f.apply(s, c);
			});
		});

		Button btnAdd = new Button("Adicionar ao hashmap");
		Edit edKey = new Edit();
		Edit edValue = new Edit();

		btnAdd.addPressListener(e -> {
			hash.put(edKey.getText(), edValue.getText());
			Stream.of(edKey, edValue).forEach(Edit::clear);
		});

		add(btn, LEFT, TOP, FILL, PREFERRED);
		add(btn2, LEFT, AFTER, FILL, PREFERRED);
		add(btn3, LEFT, AFTER, FILL, PREFERRED);

		add(c, LEFT, AFTER, FILL, FILL);

		add(btnAdd, LEFT, BOTTOM, FILL, PREFERRED);
		add(edValue, LEFT, BEFORE, FILL, PREFERRED);
		add(edKey, LEFT, BEFORE, FILL, PREFERRED);
	}

	private String serializeES(Entry<String, String> es) {
		return es.getKey().toUpperCase() + " " + es.getValue().toUpperCase();
	}
}

package br.com.softsite.streamsupport;

import java.io.Closeable;
import java.io.IOException;
import java.util.function.Supplier;

import com.totalcross.util.compile.AvailablePlatforms;
import com.totalcross.util.compile.CompilationBuilder;
import com.totalcross.util.compile.CopiedAutodeleteJar;

import totalcross.ui.MainWindow;
import totalcross.util.concurrent.Lock;

public class CompileApp {

	public static void main(String[] args) throws IOException, InterruptedException {
		String tcKey = CompilationBuilder.getenv("TOTALCROSS3_KEY");
		String tcHome = CompilationBuilder.getenv("TOTALCROSS3_HOME");
		CompilationBuilder compBuilder = new CompilationBuilder();
		compBuilder.setPlatformsTarget(AvailablePlatforms.WIN32)
			.singlePackage();

		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			switch (arg) {
			case "--tc-key":
				tcKey = args[i+1];
				i++;
				break;
			case "--tc-home":
				tcHome = args[i+1];
				i++;
				break;
			case "-n":
			case "--dry-run":
				compBuilder.setDryRun(true)
					.singlePackage(false)
					.resetPlatformsTarget();
				break;
			case "-f":
			case "--force":
				compBuilder.setMakefileTcz(false);
				break;
			default:
				throw new RuntimeException("Opção desconhecida '" + arg + "', abortando");
			}
		}

		Class<? extends MainWindow> mainWindowClass = App.class;
		try (Closeable c = createCopiedJar(mainWindowClass)) {
			compBuilder.setKey(tcKey)
				.setTotalCrossHome(tcHome)
				.setMustCompile(s -> s.contains("functional-toolbox"))
				.setMainTarget(App.class)
				.build();
		}
	}

	private static Closeable createCopiedJar(Class<? extends MainWindow> mainWindowClass) throws IOException {
		String origName = s.get();
		String destinyName = getNameMainWindow(mainWindowClass);

		if (origName.equals(destinyName)) {
			return () -> {};
		}

		return new CopiedAutodeleteJar(origName, destinyName);
	}

	private static String cleanedName(Class<? extends MainWindow> mainWindowClass) {
		return mainWindowClass.getName().replaceAll("^.*\\.", "");
	}

	private static String getNameMainWindow(Class<? extends MainWindow> mainWindowClass) {
		return "target/" + cleanedName(mainWindowClass) + ".jar";
	}

	private static Lock lock = new Lock();

	static String jarPath = null;

	private static Supplier<String> s = () -> {
		synchronized (lock) {
			if (jarPath == null) {
				jarPath = "target/stream-support-totalcross-sample-0.0.1-SNAPSHOT.jar";
				s = () -> jarPath;
			}
			return jarPath;
		}
	};
}
